package ro.siit.tau.gr3.models;

/**
 * Created by Alina on 4/4/2017.
 */
public class ContactModel {

    public String contactName;
    public String contactEmail;
    public String emailWithout;
    public String emailInvalidError;
    public String messageInvalidError;
    public String emailInvalidErrorMissing;
    public String contactSubject;
    public String contactMessage;
    public String successContact;


    public String getEmailWithout() {
        return emailWithout;
    }

    public void setEmailWithout(String emailWithout) {
        this.emailWithout = emailWithout;
    }

    public String getEmailInvalidError() {
        return emailInvalidError;
    }

    public void setEmailInvalidError(String emailInvalidError) {
        this.emailInvalidError = emailInvalidError;
    }

    public String getMessageInvalidError() {
        return messageInvalidError;
    }

    public void setMessageInvalidError(String messageInvalidError) {
        this.messageInvalidError = messageInvalidError;
    }

    public String getEmailInvalidErrorMissing() {
        return emailInvalidErrorMissing;
    }

    public void setEmailInvalidErrorMissing(String emailInvalidErrorMissing) {
        this.emailInvalidErrorMissing = emailInvalidErrorMissing;
    }




    public String getSuccessContact() {
        return successContact;
    }

    public void setSuccessContact(String successContact) {
        this.successContact = successContact;
    }



    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactSubject() {
        return contactSubject;
    }

    public void setContactSubject(String contactSubject) {
        this.contactSubject = contactSubject;
    }

    public String getContactMessage() {
        return contactMessage;
    }

    public void setContactMessage(String contactMessage) {
        this.contactMessage = contactMessage;
    }





}
