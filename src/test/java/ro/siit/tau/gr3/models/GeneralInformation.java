package ro.siit.tau.gr3.models;

/**
 * Created by Tzache on 06.04.2017.
 */
public class GeneralInformation {
    private String titleText;
    private String descriptionText;
    private String searchCateg;
    private String categoryError;

    public String getTitle() {
        return titleText;
    }

    public void setTitle(String title) {
        this.titleText = title;
    }

    public String getDescription() {
        return descriptionText;
    }

    public void setDescription(String description) {
        this.descriptionText = description;
    }



    public void setSearchCateg(String searchCateg) {
        this.searchCateg = searchCateg;
    }

    public String getSearchCateg() {

        return searchCateg;
    }

    public String getCategoryError() {
        return categoryError;
    }

    public void setCategoryError(String categoryError) {
        this.categoryError = categoryError;
    }
}
