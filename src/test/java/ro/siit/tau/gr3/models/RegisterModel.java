package ro.siit.tau.gr3.models;

/**
 * Created by Alina on 3/28/2017.
 */
public class RegisterModel {

    public String RegisterName;
    public String RegisterEmail;
    public String RegisterPassword;
    public String RegisterConfirmPassword;
    public String RegisterPasswordLong;
    public String RegisterNameError;
    public String RegisterEmailError;
    public String RegisterPasswordError;
    public String RegisterConfirmPasswordError;
    public String RegisterInvalidEmailError;
    public String RegisterPasswordErrorMismatch;
    public String RegisterTest;

    public String getRegisterTest() {
        return RegisterTest;
    }

    public void setRegisterTest(String registerTest) {
        RegisterTest = registerTest;
    }

    public String getRegisterPasswordErrorMismatch() {
        return RegisterPasswordErrorMismatch;
    }

    public void setRegisterPasswordErrorMismatch(String registerPasswordErrorMismatch) {
        RegisterPasswordErrorMismatch = registerPasswordErrorMismatch;
    }

    public String getRegisterPasswordLong() {
        return RegisterPasswordLong;
    }

    public void setRegisterPasswordLong(String registerPasswordLong) {
        RegisterPasswordLong = registerPasswordLong;
    }

    public String getRegisterInvalidEmailError() {
        return RegisterInvalidEmailError;
    }

    public void setRegisterInvalidEmailError(String registerInvalidEmailError) {
        RegisterInvalidEmailError = registerInvalidEmailError;
    }



    public String getRegisterNameError() {
        return RegisterNameError;
    }

    public void setRegisterNameError(String registerNameError) {
        RegisterNameError = registerNameError;
    }

    public String getRegisterEmailError() {
        return RegisterEmailError;
    }

    public void setRegisterEmailError(String registerEmailError) {
        RegisterEmailError = registerEmailError;
    }

    public String getRegisterPasswordError() {
        return RegisterPasswordError;
    }

    public void setRegisterPasswordError(String registerPasswordError) {
        RegisterPasswordError = registerPasswordError;
    }

    public String getRegisterConfirmPasswordError() {
        return RegisterConfirmPasswordError;
    }

    public void setRegisterConfirmPasswordError(String registerConfirmPasswordError) {
        RegisterConfirmPasswordError = registerConfirmPasswordError;
    }



    public String getRegisterEmail() {
        return RegisterEmail;
    }

    public void setRegisterEmail(String registerEmail) {
        RegisterEmail = registerEmail;
    }
    public String getRegisterName() {
        return RegisterName;
    }

    public void setRegisterName(String registerName) {
        RegisterName = registerName;
    }


    public String getRegisterPassword() {
        return RegisterPassword;
    }

    public void setRegisterPassword(String registerPassword) {
        RegisterPassword = registerPassword;
    }

    public String getRegisterConfirmPassword() {
        return RegisterConfirmPassword;
    }

    public void setRegisterConfirmPassword(String registerConfirmPassword) {
        RegisterConfirmPassword = registerConfirmPassword;
    }
}
