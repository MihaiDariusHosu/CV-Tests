package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Alina on 4/6/2017.
 */
public class RecoverPage {

    @FindBy(how = How.ID, using = "s_email")
    private WebElement emailField;

    @FindBy(how =How.XPATH, using = "//button[@type='submit']")
    private WebElement submitButton;

}
