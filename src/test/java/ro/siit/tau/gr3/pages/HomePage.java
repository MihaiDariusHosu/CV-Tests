package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by veronicaz on 3/28/2017.
 */
public class HomePage {

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[3]/h1/strong")
    private WebElement ConfirmationHomePageText;

    public String ConfirmationHomePageText() { return ConfirmationHomePageText.getText();
    }

}

