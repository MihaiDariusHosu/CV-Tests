package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import ro.siit.tau.gr3.models.LogInModel;

import java.util.List;

public class LogInPage {

    WebDriver driver;

    public LogInPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"login_open\"]")
    private WebElement login;

    @FindBy(how = How.XPATH, using = "//*[@id=\"email\"]")
    private WebElement loginEmail;

    @FindBy(how = How.XPATH, using = "//*[@id=\"password\"]")
    private WebElement loginPassword;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/form/div[3]/div[2]/button")
    private WebElement loginContinueButton;

    @FindBy(how = How.XPATH, using = "//*[@id='main']/div/div[1]/h1")
    private WebElement loginMessage;

    @FindBy(how = How.XPATH, using = "//*[@id='flashmessage']")
    private WebElement listingsMessage;

    @FindBy(how = How.XPATH, using = "//div[@id='flashmessage'][1]")
    private WebElement loginEmailError;

    @FindBy(how = How.XPATH, using = "//div[@class='actions']/a")
    private List<WebElement> registerAndForgotPassword;

    @FindBy(how = How.XPATH, using = "//div[@id='flashmessage'][2]")
    private WebElement loginPasswordError;

    public boolean getLoginEmailError(){return loginEmailError.isDisplayed();}
    public boolean getLoginPasswordError(){return loginPassword.isDisplayed();}

    public void LoginActions(LogInModel logInModel) throws InterruptedException {
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        headerPage.loginButtonClick();
        loginEmail.clear();
        loginEmail.sendKeys(logInModel.getEmail());
        loginPassword.clear();
        loginPassword.sendKeys(logInModel.getPassword());
        loginContinueButton.click();
        Assert.assertEquals(headerPage.getHiName(), "Hi didi! ·");
        //<span>Hi didi!  ·</span>

    }

    public void LoginActionsNegative(){
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        headerPage.loginButtonClick();
        loginContinueButton.click();
    }

    public void RegisterAndForgetPassword(LogInModel logInModel){
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        headerPage.loginButtonClick();

        for(int i = 0; i<registerAndForgotPassword.size(); i++){
            WebElement localRegisterPass = registerAndForgotPassword.get(i);
            String stringLocal = localRegisterPass.getText();
            System.out.println("Names : " + stringLocal + "Size of list :" + localRegisterPass.getSize());

            if (stringLocal.contains("password")){
                registerAndForgotPassword.get(1).click();
                break;
            }
        }


    }

    public boolean loginEmailErrorIsDisplayed(){return loginEmailError.isDisplayed();}

    public boolean loginPasswordErrorIsDisplayed(){return loginPasswordError.isDisplayed();}

    public boolean loginMessageIsDisplayed() {
       return loginMessage.isDisplayed();
    }

    public boolean listingsMessageIsDisplayed(){
      return listingsMessage.isDisplayed();
    }

    public void loginHardCode(){
        loginEmail.sendKeys("baiat@baiat.com");
        loginPassword.sendKeys("baiat");
        loginContinueButton.click();
    }


}
