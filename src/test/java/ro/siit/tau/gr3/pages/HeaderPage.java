package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import ro.siit.tau.gr3.models.SearchModel;

/**
 * Created by Tzache on 28.03.2017.
 */
public class HeaderPage {
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Login')]")
    private WebElement loginButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Register for a free account')]")
    private WebElement registerButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Publish your ad for free')]")
    private WebElement publishAdd;

    @FindBy(how = How.XPATH, using = "//*[@id=\"header\"]/form/div[1]/div[1]/div/label")
    private WebElement searchField;

    @FindBy(how = How.XPATH, using = "//*[@id=\"header\"]/div[3]/ul/li[1]/span")
    private WebElement hiName;

    @FindBy(how = How.XPATH,using = "//button[contains(text(),'Search')]")
    private WebElement searchButton;

    @FindBy(how = How.XPATH, using="//select[@id='sCategory']")
    private WebElement dropSearch;

    @FindBy(how= How.XPATH, using = "//select[@id='sCategory']/option[2]")
    private WebElement personals;


    public String getHiName(){
       return hiName.getText();
    }
    public void loginButtonClick(){
        loginButton.click();
    }
    public void registerButtonClick(){
        registerButton.click();
    }
    public void publishAddClick(){
        publishAdd.click();
    }

    public void searchButtonClick(){searchButton.click();}

    public void dropSearchClick(){dropSearch.click();}

    public String personalsCategText(){personals.click();
        return personals.getText();}



    public void searchDrop(SearchModel searchModel) throws InterruptedException {
        Select dropdown = new Select(dropSearch);
        dropdown.selectByVisibleText(searchModel.getSearchCateg());

    }


}
