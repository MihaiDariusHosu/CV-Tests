package ro.siit.tau.gr3.pages;

import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;
import ro.siit.tau.gr3.Utils.RandomEmail;
import ro.siit.tau.gr3.models.RegisterModel;

import java.util.List;
import java.util.Random;

/**
 * Created by Alina on 3/28/2017.
 */
public class RegisterPage {

    public RegisterPage() {
    }

    private Assertion hardAssert = new Assertion();

    private SoftAssert softAssert = new SoftAssert();

    WebDriver driver;

   public RegisterPage (WebDriver driver){
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using =  "//*[@id=\"s_name\"]")
    private WebElement RegisterName;

     @FindBy(how = How.XPATH, using = "//*[@id=\"s_email\"]")
    private WebElement RegisterEmail;

     @FindBy(how = How.XPATH, using = "//*[@id=\"s_password\"]")
    private WebElement RegisterPassword;

     @FindBy(how = How.XPATH, using =  "//*[@id=\"s_password2\"]")
    private WebElement RegisterConfirmPassword;

     @FindBy(how =How.XPATH, using = "//button[@type='submit']")
    private WebElement RegisterCreateButton;

     @FindBy(how= How.XPATH, using = "//*[@id=\"main\"]/div/div[1]/h1")
     private WebElement registerMessage;

     @FindBy(how = How.XPATH, using = "//*[@id=\"flashmessage\"]")
     private WebElement registerSuccessfulMessage;

     @FindBy(how = How.XPATH, using = "//ul[@id='error_list']//label")
     private List<WebElement> allInputErrors;

     @FindBy(how = How.XPATH, using = "//*[@id=\"error_list\"]/li[1]/label")
     private WebElement nameError;

     @FindBy(how = How.XPATH, using = "//*[@id=\"error_list\"]/li[2]/label")
     private WebElement emailError;

     @FindBy(how = How.XPATH, using = "//*[@id=\"error_list\"]/li[3]/label")
     private WebElement passwordError;

     @FindBy(how = How.XPATH, using = "//*[@id=\"error_list\"]/li/label")
     private WebElement passworErrorLong;

     @FindBy(how = How.XPATH, using = "//*[@id=\"error_list\"]/li[4]/label")
     private WebElement confirmPasswordError;

     @FindBy(how = How.XPATH, using = "//label[contains(.,'Invalid')]")
     private WebElement emailInvalidError;

     @FindBy(how = How.XPATH, using = "//label[@for='s_password2']")
     private WebElement passwordMismatch;

     public String passwordMismatch(){
         return passwordMismatch.getText();
     }

     public String passwordErrroLong(){
       return passworErrorLong.getText();
     }

     public String emailInvalidError(){
         return emailInvalidError.getText();
     }
     public String nameInvalidError(){
         return nameError.getText();
     }

    public boolean isRegisterSuccessMessageDisplayed(){
   return registerSuccessfulMessage.isDisplayed();
}
    public void assertRegisterMessage(){
         registerSuccessfulMessage.getText();
     }

     public void RegisterActions(RegisterModel registerModel){
         HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
         RandomEmail randomEmail = PageFactory.initElements(driver, RandomEmail.class);
         headerPage.registerButtonClick();
         RegisterName.clear();
         RegisterName.sendKeys(registerModel.getRegisterName());
         RegisterEmail.clear();
         RegisterEmail.sendKeys(randomEmail.random());
         RegisterPassword.clear();
         RegisterPassword.sendKeys(registerModel.getRegisterPassword());
         RegisterConfirmPassword.sendKeys(registerModel.getRegisterConfirmPassword());
         RegisterCreateButton.click();
     }

     public void RegisterActionsComplicatedNegativeFlow( ){
         HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
         headerPage.registerButtonClick();
         RegisterCreateButton.click();
      for (int i=0; i<allInputErrors.size(); i++){
          WebElement errorsDisplayed = allInputErrors.get(i);
          String errorsDisplayedfor = errorsDisplayed.getAttribute("for");
          System.out.println("for attribute on error is : " + errorsDisplayedfor);

      if (errorsDisplayedfor.equals("s_name")){
          Assert.assertTrue(nameError.isDisplayed());
      }

      if(errorsDisplayed.equals("s_email")){
          Assert.assertTrue(emailError.isDisplayed());
      }

      if(errorsDisplayed.equals("s_password")){
          Assert.assertTrue(passwordError.isDisplayed());
      }
      if(errorsDisplayed.equals("s_password2"))
      {
          Assert.assertTrue(confirmPasswordError.isDisplayed());
      }
      }

         System.out.println("List size of values : " + allInputErrors.size());
     }
     public void RegisterNegativeFlow(RegisterModel registerModel){
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        headerPage.registerButtonClick();
         RegisterName.sendKeys(registerModel.getRegisterName());
         RegisterEmail.sendKeys(registerModel.getRegisterEmail());
         RegisterPassword.sendKeys(registerModel.getRegisterPassword());
         RegisterConfirmPassword.sendKeys(registerModel.getRegisterConfirmPassword());
         RegisterCreateButton.click();

     }

     public boolean registerMessageisDisplayed(){
         return registerMessage.isDisplayed();
     }


}
