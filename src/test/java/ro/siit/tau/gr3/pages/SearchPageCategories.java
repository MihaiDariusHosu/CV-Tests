package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tzache on 30.03.2017.
 */
public class SearchPageCategories {
    @FindBy(how = How.XPATH, using ="//div[@class='listing-attributes']//span[@class='category']")
    private List<WebElement> users;
    @FindBy(how = How.XPATH, using= "//div[@class='resp-wrapper']//h1")
    private WebElement category;

    @FindBy(how = How.XPATH, using = "//*[@id='flashmessage']")
    private WebElement successMessage;

    public ArrayList<String> usersFound() {
        ArrayList<String> usersList = new ArrayList<String>();
        for (WebElement user : users) {
            usersList.add(user.getText());
        }
        System.out.println(usersList);
        return usersList;
    }
    public String categoryTitle(){return category.getText();}
    public boolean successMessagDisplayed(){
        return successMessage.isDisplayed();
    }
}
