package ro.siit.tau.gr3.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import ro.siit.tau.gr3.Utils.RandomEmail;

/**
 * Created by Tzache on 01.04.2017.
 */
public class SearchPage {
    WebDriver driver;

    public SearchPage (WebDriver driver){
        this.driver = driver;
    }


    @FindBy(how = How.XPATH, using = "//span[@itemprop='title']")
    private WebElement searchResult;

    @FindBy(how = How.XPATH, using = "//button[@class='sub_button']")
    private WebElement subscribeButton;

    @FindBy(how =How.ID, using = "alert_email")
    private WebElement EnterYourEmail;

    public void clickSubscribeButton(){
        subscribeButton.click();
    }



    public boolean searchResultTextIsDisplayed(){return searchResult.isDisplayed();}
    public String searchResultText(){return searchResult.getText();}

    public void NegativeFlowPopUP () throws InterruptedException{

        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        headerPage.searchButtonClick();
        searchPage.clickSubscribeButton();
        Thread.sleep(1000);
        Alert alert = driver.switchTo().alert();
        alert.accept();

    }

    public void PositiveFlowSubscribe() throws InterruptedException{
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        RandomEmail randomEmail = PageFactory.initElements(driver, RandomEmail.class);
        headerPage.searchButtonClick();
        EnterYourEmail.clear();
        EnterYourEmail.sendKeys(randomEmail.random());
        clickSubscribeButton();
        Thread.sleep(500);
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }
}
