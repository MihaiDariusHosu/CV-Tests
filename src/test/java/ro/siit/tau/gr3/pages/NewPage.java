package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import ro.siit.tau.gr3.models.GeneralInformation;

/**
 * Created by Tzache on 06.04.2017.
 */
public class NewPage {
    private WebDriver driver;

    @FindBy(how = How.CSS, using = "select#catId")
    private WebElement categoryList;

    @FindBy(how = How.XPATH, using = "//div[@class = 'controls']//input[@type='text']")
    private WebElement title;

    @FindBy(how = How.XPATH, using = "//div[@class = 'controls']//textarea")
    private WebElement description;

    @FindBy(how = How.XPATH, using = "//div[@class = 'controls']//button[@type='submit']")
    private WebElement submitButton;

   // @FindBy(how = How.XPATH, using = "//*[@id='error_list']/li/label")
   // private WebElement categoryError;

    @FindBy(how = How.ID, using = "//*[@id='flashmessage']")
    private WebElement flashError;

    @FindBy(how = How.XPATH, using = "//div[@class='header']//h1")
    private WebElement publishHeader;

    @FindBy(how = How.XPATH, using = "//*[@id='error_list']/li/label")
    private WebElement categoryError;

    @FindBy(how = How.XPATH, using = "//*[@id='flashmessage']")
    private WebElement tilteDesriptionError;


    public void searchDrop(GeneralInformation generalInformation) throws InterruptedException {
        Select dropdown = new Select(categoryList);
        dropdown.selectByVisibleText(generalInformation.getSearchCateg());
    }

    public void dropClick(){
        categoryList.click();

    }

    public void fieldsCompletion(GeneralInformation generalInformation) {
        publishHeader.click();
        title.sendKeys(generalInformation.getTitle());
        description.sendKeys(generalInformation.getDescription());
    }

    public void submitPublish(){submitButton.click();}

  /** For negative test purposes
   * public String errorText(){
      return categoryError.getText();
    }

    public boolean titleDescriptionErrorDisplayed(){
        return tilteDesriptionError.isDisplayed();
    }*/
}




