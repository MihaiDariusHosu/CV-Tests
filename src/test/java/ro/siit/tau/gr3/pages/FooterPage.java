package ro.siit.tau.gr3.pages;

import com.gargoylesoftware.htmlunit.html.DomElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;

/**
 * Created by veronicaz on 3/28/2017.
 */

public class FooterPage {

   //find the footer Login
    @FindBy(id="footer")
    private WebElement loginButton;


    //click the login button in the footer
    public void loginButtonClick() {loginButton.click();

    }
    //find the footer Register for free account
    @FindBy(id="footer>a")
    private WebElement RegisterAccountButton;

    //click the Register for free account
    public void RegisterAccountButton() {
        RegisterAccountButton.click();
    }

    //find the footer Publish your add for free
    @FindBy(how = How.XPATH, using = "//*[@id='footer']/div/ul[1]/li[3]/a")
    private WebElement PublishAddforfreeButton;

    //click the footer PublishAddforfreeButton(){
    public void clickPublishAddforfreeButton() {
        PublishAddforfreeButton.click();
    }

    //find the footer Contact button
    @FindBy(how = How.XPATH, using = "//*[@id='footer']/div/ul[2]/li/a")
    private WebElement ContactButton;


    //click theFooterContactButton (){
    public void clickContactButton(){
        ContactButton.click();
    }

    //select English language
    @FindBy(how = How.XPATH, using ="//*[@id='en_US']")
    public WebElement EnglishLanguageButton;
    //select English Language
    public void EnglishLanguageButton(){
        EnglishLanguageButton.click();
    }

    public void ContactClick(){
        ContactButton.click();
    }

}




