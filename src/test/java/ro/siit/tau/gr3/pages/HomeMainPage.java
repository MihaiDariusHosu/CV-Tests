package ro.siit.tau.gr3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by Majin on 3/30/2017.
 */
public class HomeMainPage {

    WebDriver driver;

    public HomeMainPage (WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]")
    private WebElement mainDiv;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[1]/ul/li/ul/li")
    private List<WebElement> personalsList;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[3]")
    private WebElement latestAds;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'listing-basicinfo')]/a")
    private List<WebElement> latestAdsListTitle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[3]/div[1]/span/a[1]")
    private WebElement listView;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[3]/div[1]/span/a[2]")
    private WebElement gridView;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div[3]/p/a")
    private WebElement seeAllListenings;

    @FindBy(how = How.XPATH, using = "//div[@class='listing-attributes']//span[@class='location']")
    private List<WebElement> listLocation;

    @FindBy(how = How.XPATH, using =  "//span[@class='category']")
    private List<WebElement> listCategory;

    public void listLatestAdsActions() {
        System.out.println("size of the list is : " + latestAdsListTitle.size());

        for (int i = 0; i < latestAdsListTitle.size(); i++) {
            WebElement localAdsList = latestAdsListTitle.get(i);
            String stringLocalAdsListLink = localAdsList.getAttribute("href");
            String stringLocalAdsListTitle = localAdsList.getAttribute("title");
            System.out.println();

            for (int u = 0; u < listLocation.size(); u++) {
                WebElement localLocations = listLocation.get(u);
                String stringLocalLocations = localLocations.getText();


                for (int y = 0; y < listCategory.size(); y++) {
                    WebElement localCategory = listCategory.get(y);
                    String stringLocalCategory = localCategory.getText();

                    System.out.println("Link : " + stringLocalAdsListLink + "\n" +
                        "Title : " + stringLocalAdsListTitle +  "\n" +  "Location : " + stringLocalLocations + "\n" + "Category : " + stringLocalCategory + "\n");
                    // question about the break sequence

                }
                break;
            }
            break;
        }


    }

    public void listGridStateActions(){
        //question about is enabled
        listView.click();
        if(listView.isEnabled()){
            System.out.println("ListView is enabled correctly ");
        }
        gridView.click();
        if(gridView.isEnabled()){
            System.out.println("GridView is enabled correctly ");
        }
        }

        public void PersonalListActions(){
            System.out.println("List of Personals is : ");
            System.out.println();
        for (int i = 0; i<personalsList.size(); i++){
            WebElement localPersonalList = personalsList.get(i);
            String stringLocalPersonalList = localPersonalList.getText();
            System.out.println(stringLocalPersonalList);
        }


        }


    }



