package ro.siit.tau.gr3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import ro.siit.tau.gr3.models.ContactModel;

/**
 * Created by Alina on 4/4/2017.
 */
public class ContactPage {

    WebDriver driver;

    public ContactPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(how = How.ID, using = "yourName")
    private WebElement yourName;

    @FindBy(how = How.ID, using = "yourEmail")
    private WebElement yourEmail;

    @FindBy(how = How.ID, using = "subject")
    private WebElement subject;

    @FindBy(how = How.ID, using = "message")
    private WebElement message;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Your email')]")
    private WebElement successContact;

    @FindBy(how = How.XPATH, using = "//label[contains(text(),'Email:')]")
    private WebElement emailMissingError;

    @FindBy(how =How.XPATH, using = "//label[contains(text(),'Message:')]")
    private WebElement messageMissingError;

    public boolean isDisplayedEmailMissingError(){
       return emailMissingError.isDisplayed();
    }

    public boolean isDisplayedMessageMissingError(){
        return messageMissingError.isDisplayed();
    }


    ////*[@id="flashmessage"]/text()

    public String successContact(){
        return successContact.getText();
    }

    public boolean isDisplayedSuccessContact(){
      return successContact.isDisplayed();
    }
    //<div id="flashmessage" class="flashmessage flashmessage-ok"><a class="btn ico btn-mini ico-close">x</a>Your email has been sent properly. Thank you for contacting us!</div>

public void ContactHappyFlow(ContactModel contactModel){
    FooterPage footerPage = PageFactory.initElements(driver, FooterPage.class);
    footerPage.clickContactButton();
    yourName.sendKeys(contactModel.contactName);
    yourEmail.sendKeys(contactModel.contactEmail);
    subject.sendKeys(contactModel.contactSubject);
    message.sendKeys(contactModel.contactMessage);
    submitButton.click();
}

public void ContactNegativeFlow(){
    FooterPage footerPage = PageFactory.initElements(driver, FooterPage.class);
    footerPage.clickContactButton();
    submitButton.click();

}
}
