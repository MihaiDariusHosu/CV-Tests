package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.models.GeneralInformation;
import ro.siit.tau.gr3.pages.HeaderPage;
import ro.siit.tau.gr3.pages.LogInPage;
import ro.siit.tau.gr3.pages.NewPage;
import ro.siit.tau.gr3.pages.SearchPageCategories;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Tzache on 06.04.2017.
 */
public class PublishTest extends BaseTest{
    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @DataProvider(name="PublishTest")
    public Iterator<Object[]> publish() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("Publish");

        for (File f : files) {
            GeneralInformation r = objectMapper.readValue(f, GeneralInformation.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }

    @Test (dataProvider = "PublishTest")
    public void publishTest(GeneralInformation generalInformation) throws InterruptedException{
        NewPage newPage = PageFactory.initElements(driver,NewPage.class);
        HeaderPage headerPage = PageFactory.initElements(driver,HeaderPage.class);
        LogInPage logInPage = PageFactory.initElements(driver,LogInPage.class);
        SearchPageCategories searchPageCategories = PageFactory.initElements(driver,SearchPageCategories.class);


        headerPage.loginButtonClick();
        logInPage.loginHardCode();
        headerPage.publishAddClick();

        newPage.searchDrop(generalInformation);
        newPage.fieldsCompletion(generalInformation);
        newPage.submitPublish();

        /** Negative not working

        String categoryError = generalInformation.getCategoryError();
        if (newPage.errorTextDisplayed()){
                Assert.assertEquals(categoryError,newPage.errorText());
        }
        if (newPage.titleDescriptionErrorDisplayed() == true){
            Assert.assertTrue(newPage.titleDescriptionErrorDisplayed());
        }*/
        //Thread.sleep(2000);
        Assert.assertTrue(searchPageCategories.successMessagDisplayed());


    }


}
