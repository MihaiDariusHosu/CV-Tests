package ro.siit.tau.gr3.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ro.siit.tau.gr3.Utils.Browsers;
import ro.siit.tau.gr3.Utils.WebBrowsers;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alina on 3/28/2017.
 */
public class BaseTest {

    public WebDriver driver;

    @BeforeMethod
    public void setUp() {
        driver = WebBrowsers.getDriver(Browsers.CHROME);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        String handle = driver.getWindowHandle();
        System.out.println(handle.toString());
    }

    @AfterMethod
    public void quit() {
        driver.quit();
    }

    File[] getListOfFiles(String directoryName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File directory = new File(classLoader.getResource(directoryName).getFile());
        File[] files = directory.listFiles();
        System.out.println("Found " + files.length + " files in " + directoryName + " folder");
        return files;
    }




}
