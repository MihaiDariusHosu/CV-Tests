package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.models.ContactModel;
import ro.siit.tau.gr3.models.RegisterModel;
import ro.siit.tau.gr3.pages.ContactPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ContactTest extends BaseTest {
    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @DataProvider(name = "ContactHappyTest")
    public Iterator<Object[]> RegisterNegative() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("Contact");
        for (File f : files) {
            ContactModel r = objectMapper.readValue(f, ContactModel.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }
    @Test(dataProvider = "ContactHappyTest")
    public void ContactTestHappy(ContactModel contactModel){
        ContactPage contactPage = PageFactory.initElements(driver, ContactPage.class);
        contactPage.ContactHappyFlow(contactModel);
        Assert.assertTrue(contactPage.isDisplayedSuccessContact());
    }
    @Test()
    public void ContactSadTest() throws InterruptedException{
        ContactPage contactPage = PageFactory.initElements(driver, ContactPage.class);
        contactPage.ContactNegativeFlow();
        Assert.assertTrue(contactPage.isDisplayedEmailMissingError());
        Assert.assertTrue(contactPage.isDisplayedMessageMissingError());
    }

}
