package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.models.LogInModel;
import ro.siit.tau.gr3.pages.LogInPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Alina on 3/28/2017.
 */
public class LoginTest extends BaseTest {

    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @DataProvider(name="LoginTest")
    public Iterator<Object[]> BuyItemSearch() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("LogIn");
        for (File f : files) {
            LogInModel r = objectMapper.readValue(f, LogInModel.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }
@Test(dataProvider = "LoginTest")
    public void LoginTestHappyFlow(LogInModel logInModel) throws InterruptedException{
        LogInPage logInPage = PageFactory.initElements(driver, LogInPage.class);
        logInPage.LoginActions(logInModel);
}
@Test(testName = "LogInTestNegative")
    public void LoginTestNegativeFlow() throws InterruptedException{
        LogInPage logInPage = PageFactory.initElements(driver, LogInPage.class);
        logInPage.LoginActionsNegative();
    Assert.assertTrue(logInPage.loginEmailErrorIsDisplayed());
    Assert.assertTrue(logInPage.loginPasswordErrorIsDisplayed());
}
   /** @Test
    public void ForgotAndRegisterTest(){

        LogInPage logInPage = PageFactory.initElements(driver, LogInPage.class);
        logInPage.RegisterAndForgetPassword();
    }*/


}
