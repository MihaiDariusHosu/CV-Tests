package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.Utils.RandomEmail;
import ro.siit.tau.gr3.models.LogInModel;
import ro.siit.tau.gr3.models.RegisterModel;
import ro.siit.tau.gr3.pages.RegisterPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Alina on 3/28/2017.
 */
public class RegisterTest extends BaseTest {


    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @DataProvider(name = "RegisterTest")
    public Iterator<Object[]> BuyItemSearch() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("Register");
        for (File f : files) {
            RegisterModel r = objectMapper.readValue(f, RegisterModel.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }

    @DataProvider(name = "NegativeRegister")
    public Iterator<Object[]> RegisterNegative() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("RegisterFail1");
        for (File f : files) {
            RegisterModel r = objectMapper.readValue(f, RegisterModel.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }


    @Test(dataProvider = "RegisterTest")
    public void RegisterActionsHappyFlow(RegisterModel registerModel) {
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.RegisterActions(registerModel);
        Assert.assertTrue(registerPage.isRegisterSuccessMessageDisplayed());
    }

    @Test(testName = "RegisterAllNegative")
    public void NegativeFlowRegisterTest() {
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.RegisterActionsComplicatedNegativeFlow();
    }

    @Test(dataProvider = "NegativeRegister")
    public void NegativeRegisterTest(RegisterModel registerModel) {
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.RegisterNegativeFlow(registerModel);
        if (registerModel.getRegisterTest().equals("1")) {
            Assert.assertEquals(registerModel.getRegisterInvalidEmailError(), registerPage.emailInvalidError());
        } else if (registerModel.getRegisterTest().equals("2")) {
            Assert.assertEquals(registerModel.getRegisterNameError(), registerPage.nameInvalidError());
        } else if (registerModel.getRegisterTest().equals("3"))
        {Assert.assertEquals(registerModel.getRegisterPasswordLong(), registerPage.passwordErrroLong());}
        else if (registerModel.getRegisterTest().equals("4")){
            Assert.assertEquals(registerModel.getRegisterPasswordErrorMismatch(), registerPage.passwordMismatch());
        }
    }
}
