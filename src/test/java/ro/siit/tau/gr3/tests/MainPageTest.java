package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.deser.Deserializers;
import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.pages.HomeMainPage;

import java.awt.print.PageFormat;

/**
 * Created by Majin on 3/30/2017.
 */
public class MainPageTest extends BaseTest {

    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @Test
    public void HomePageMainListTest(){
        HomeMainPage homeMainPage = PageFactory.initElements(driver, HomeMainPage.class);
        homeMainPage.listLatestAdsActions();
    }

    @Test
    public void GridListButtonState(){
        HomeMainPage homeMainPage = PageFactory.initElements(driver, HomeMainPage.class);
        homeMainPage.listGridStateActions();
    }

    @Test
    public void ListOfPersonalTest(){
        HomeMainPage homeMainPage = PageFactory.initElements(driver, HomeMainPage.class);
        homeMainPage.PersonalListActions();
    }

   /*
    <a href="http://osc-tausandbox.rhcloud.com/index.php?sShowAs=gallery" class="grid-button" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span>Grid</span></a>
     <a href="http://osc-tausandbox.rhcloud.com/index.php?sShowAs=gallery" class="grid-button" data-class-toggle="listing-grid" data-destination="#listing-card-list"><span>Grid</span></a>
     */



}
