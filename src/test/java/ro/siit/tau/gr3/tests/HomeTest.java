package ro.siit.tau.gr3.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import ro.siit.tau.gr3.pages.HomePage;

/**
 * Created by veronicaz on 3/30/2017.
 */
public class HomeTest {
    private WebDriver driver;
    HomePage homePage = PageFactory.initElements(driver, HomePage.class);

    public void HomeTest(){
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        Assert.assertEquals(homePage.ConfirmationHomePageText(), "Latest Listings ");
    }


}

