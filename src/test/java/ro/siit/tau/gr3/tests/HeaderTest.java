package ro.siit.tau.gr3.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.pages.HeaderPage;
import ro.siit.tau.gr3.pages.LogInPage;
import ro.siit.tau.gr3.pages.RegisterPage;

/**
 * Created by Tzache on 28.03.2017.
 */
public class HeaderTest extends BaseTest {
    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @Test
    public void headerTest(){
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        LogInPage loginPage = PageFactory.initElements(driver,LogInPage.class);
        RegisterPage registerPage= PageFactory.initElements(driver,RegisterPage.class);
        headerPage.loginButtonClick();
        Assert.assertTrue(loginPage.loginMessageIsDisplayed());
        driver.navigate().back();
        headerPage.registerButtonClick();
        Assert.assertTrue(registerPage.registerMessageisDisplayed());
        driver.navigate().back();
        headerPage.publishAddClick();
        Assert.assertTrue(loginPage.listingsMessageIsDisplayed());
    }
}
