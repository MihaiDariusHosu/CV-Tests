package ro.siit.tau.gr3.tests;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Alina on 4/6/2017.
 */
public class TestListener extends TestListenerAdapter {

    public void onTestStart(ITestResult result){
        String testMethodName = result.getMethod().getMethodName();
        String testDescription = result.getMethod().getDescription();
        System.out.println();
        System.out.println("Start Test " + testMethodName + testDescription);
    }


}
