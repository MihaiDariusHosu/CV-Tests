package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.models.SearchModel;
import ro.siit.tau.gr3.pages.HeaderPage;
import ro.siit.tau.gr3.pages.SearchPage;
import ro.siit.tau.gr3.pages.SearchPageCategories;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Tzache on 28.03.2017.
 */
public class SearchTest extends BaseTest {

    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
            }

    @DataProvider(name="SearchTest")
    public Iterator<Object[]> BuyItemSearch() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();

        File[] files = getListOfFiles("Search");
        for (File f : files) {
            SearchModel r = objectMapper.readValue(f, SearchModel.class);
            dp.add(new Object[]{r});
        }
        return dp.iterator();
    }
    @Test
     public void searchTest0() throws InterruptedException {
        SearchPage searchPage = PageFactory.initElements(driver,SearchPage.class);
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        SearchPageCategories searchPageCategories = PageFactory.initElements(driver,SearchPageCategories.class);

        headerPage.searchButtonClick();
        Assert.assertTrue(searchPage.searchResultTextIsDisplayed());
        driver.navigate().back();
        String personalText = headerPage.personalsCategText();
        headerPage.searchButtonClick();
        Assert.assertEquals(personalText.trim(),searchPageCategories.categoryTitle().trim());
  }

   @Test(dataProvider = "SearchTest")
   public void searchTest2(SearchModel searchModel) throws InterruptedException{
       HeaderPage headerPage=PageFactory.initElements(driver,HeaderPage.class);
       SearchPageCategories searchPageCategories= PageFactory.initElements(driver,SearchPageCategories.class);
       headerPage.searchDrop(searchModel);
       headerPage.searchButtonClick();
       Assert.assertEquals(searchModel.getSearchCateg().trim(),searchPageCategories.categoryTitle());
       secondSearchAssert();
   }



    public void secondSearchAssert(){
        SearchPageCategories searchPageCategories = PageFactory.initElements(driver,SearchPageCategories.class);
        String searchValue = searchPageCategories.categoryTitle();
        searchValue = searchValue.toLowerCase();
        for (String categ : searchPageCategories.usersFound()) {
          categ = categ.toLowerCase();
          Assert.assertEquals(searchValue.trim(),categ);
      }
}
}
