package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.deser.Deserializers;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.pages.HeaderPage;
import ro.siit.tau.gr3.pages.SearchPage;

/**
 * Created by Alina on 4/4/2017.
 */
public class SubscribeTest extends BaseTest {

    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @Test(testName = "NegativePopUpTest")
    public void SubscribeNegativePopTest() throws InterruptedException{
     SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
     searchPage.NegativeFlowPopUP();
    }

    @Test()
    public void SubscribePositiveFlow() throws InterruptedException{
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        searchPage.PositiveFlowSubscribe();
    }
}



