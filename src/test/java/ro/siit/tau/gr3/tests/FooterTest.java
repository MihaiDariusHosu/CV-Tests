package ro.siit.tau.gr3.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr3.pages.FooterPage;
import ro.siit.tau.gr3.pages.HeaderPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by veronicaz on 3/28/2017.
 */
public class FooterTest extends BaseTest {
    @BeforeMethod
    public void goTo() {
        driver.get("http://osc-tausandbox.rhcloud.com/");
    }

    @DataProvider(name = "FooterDataProvider")
    public Iterator<Object[]> FooterDataProvider() {

        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new Object[]{""});
        return dp.iterator();
    }

    //assert English language in the footer page
    @Test
    public void FooterTest() {
        FooterPage footerPage = PageFactory.initElements(driver, FooterPage.class);
        footerPage.EnglishLanguageButton.click();
       Assert.assertTrue(footerPage.EnglishLanguageButton.isDisplayed());
    }
}